import '../scss/main.scss'

import './form-submission-handler'

import AOS from 'aos'
import Pushbar from './pushbar'

AOS.init({ once: true })

new Pushbar({
  blur: false,
  overlay: true
})

setTimeout(() => {
  document.querySelector('.pushbar').classList.add('ready')
}, 500)
